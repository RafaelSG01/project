/*Navbar js*/
jQuery(function ($) {

  if(window.location.hash == '#/material_de_estudos'){
    $(".sidebar-submenu").slideDown(); 
  }

  $(".sidebar-dropdown > a").click(function() {
    $(".nav-item").removeClass("active");
    $(".sidebar-submenu").slideUp(200);    
    if (
      $(this)
        .parent()
        .hasClass("active")
    ) {
      $(".nav-item").removeClass("active");
      $(this)
        .parent()
        .removeClass("active");
    } else {
      $(".sidebar-dropdown").removeClass("active");
      $(this)
        .next(".sidebar-submenu")
        .slideDown(200);
      $(this)
        .parent()
        .addClass("active");
    }
  });

  $("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
  });
  $("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
  });

  if(window.location.hash != '#/'){
    $("#list1").removeClass("active");
  } else if(window.location.hash != '#/' && window.location.hash == '#/material_de_estudos'){
    $("#list1").removeClass("active");
    $("#list2").addClass("active");  
  } 
  $(".nav-item").click(function() {
    $(".nav-item").removeClass("active");
    $(".sidebar-dropdown").removeClass("active");
    $(".sidebar-submenu").slideUp(200);   
    $(this).addClass('active');
  });


});


