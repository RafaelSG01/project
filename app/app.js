/**
* Definição dos módulos, controllers, rotas e diretivas da aplicação
* @author Equipe de Desenvolvimento
* @version 1.0
* @copyright UFC/UNASUS (Nuteds)
*/

// Definindo namespace structure para uso global e adicionado os módulos que serão usados na aplicação
var structure = angular.module('structure', ['ngRoute', 'ui.bootstrap', 'ngDragDrop', 'ngSanitize']);

/*
* Controllers
*/

// Responsável por fazer o parser do JSON e verificar os parametros atuais
structure.controller('MainCtrl', ['$rootScope','$http', '$routeParams', function($rootScope,$http,$routeParams) {
  $http.get('data/structure.json')
  .success(function(data) {
    $rootScope.unidades = data.unidades;
    $rootScope.url = 'views/unidade_1/modal/referencias.html';
  })
  .error(function(data) {
    console.log('Erro ao capturar os dados do JSON');
  });
}]);

// Accordion
structure.controller('accordionController', ['$scope', function($scope){
  $scope.oneAtATime = true;
}]);

// Carousel
structure.controller('magazineController', ['$scope', function($scope){
  $scope.myInterval = -100;

  $scope.init = function(path, pages, ext) {
    $scope.path = path;
    $scope.pages = pages;
    $scope.ext =  ext;
    var path = $scope.path;
    var pages = $scope.pages;
    var ext = $scope.ext;
    var slides = $scope.slides = [];
    for (var i=0; i<pages; i++) {
      slides.push({
        image: 'assets/images/'+ path +'/p'+i+'.'+ ext,
      });
    }
  };
}]);

// Responsável por injetar na view o arquivo correspondente a urlUnidade requisitada.
structure.controller('includeController', ['$scope','$rootScope', '$http','$routeParams','$sce', function($scope,$rootScope,$http, $routeParams,$sce) {
    if($rootScope.unidades){
      escolherPaginaCorreta($scope,$rootScope,$http, $routeParams);
      progressbar($scope, $rootScope, $http, $routeParams);
    }else{ //se a conexão não for boa, o json não estará carregado a esse ponto, então fazemos a requisição novamente
      $http.get('data/structure.json')
      .success(function(data) {
        $rootScope.unidades = data.unidades;
        escolherPaginaCorreta($scope,$rootScope,$http, $routeParams);
        progressbar($scope, $rootScope, $http, $routeParams);
      })
      .error(function(data) {
        console.log('Erro ao capturar os dados do JSON');
      });
    }

    $scope.trustSrc = function(src) {
      return $sce.trustAsResourceUrl(src);
    }

}]);

function escolherPaginaCorreta($scope,$rootScope,$http, $routeParams){   
   
  if($routeParams.unidadeID){
    // o menu de tópico será mostrado em qualquer página que tenha unidade na url
    $rootScope.mostrarMenuUnidades = true;

    //Setar variáveis no rootScope, assim podem ser usadas em todos os outros controllers
    $rootScope.unidadeAtual = $rootScope.unidades[$routeParams.unidadeID-1];


    // se a url nao tiver topicoID, carregamos o sumario da unidade
    if($routeParams.topicoID){
      $rootScope.topicoAtual   = $rootScope.unidadeAtual.topicos[$routeParams.topicoID-1];

      // para usar no menu e marcar selecionada
      $.each($rootScope.unidadeAtual.topicos, function (index,topico){
        topico.selecionada = $rootScope.topicoAtual == topico
      })

      $rootScope.topicoAtual    = $rootScope.unidadeAtual.topicos[$routeParams.topicoID-1];
      $rootScope.paginaAtualID  = parseInt($routeParams.paginaID);
      
      var path = 'views/unidade_'+$routeParams.unidadeID+'/topico_'+$routeParams.topicoID+'/' +$routeParams.paginaID+'.html';
      localStorage.setItem("local", document.URL);    
      localStorage.setItem("pasta", path);
      
      $scope.getPartial   = function () {return path;}
    }else{
      // Regra para jogo (refatorar depois)
      if ($routeParams.jogo) {
        var path = 'views/unidade_'+$routeParams.unidadeID + '/jogo/1.html';
        $scope.getPartial   = function () {return path;} 
      } else {
        var path = 'views/curso/sumario_modulo.html';
        $scope.getPartial   = function () {return path;}
      }
    }
  }else{
    // nas telas sem unidade na url, não será mostrada o menu de tópicos
    $rootScope.mostrarMenuUnidades = true;
  }
  

}

structure.controller('PaginationCtrl',['$scope', '$rootScope',  function($scope,$rootScope){

  var topicos = $rootScope.unidadeAtual.topicos;
  var topicoAtual = topicos[$rootScope.topicoAtual.id - 1];

  /**
  * quantidade total de páginas
  */
  $scope.totalPaginas = 0;
  $.each(topicos, function(index,topico){
    $scope.totalPaginas += parseInt(topico.quantidadePaginas);
  });

  /**
  * página atual (no contexto da páginação)
  * soma a quantidade de páginas dos tópicos anteriores com a página atual
  */
  $scope.paginacaoAtual = 0;
  for (var i = 0; i<topicoAtual.id-1; i++){
    $scope.paginacaoAtual += parseInt(topicos[i].quantidadePaginas);
  }
  $scope.paginacaoAtual += $rootScope.paginaAtualID;

  /**
  * url página anterior
  */
  var urlUnidade = '#/unidade_'+$rootScope.unidadeAtual.id+'/';
  $scope.urlPaginaAnterior = urlUnidade;
  //quando a página é maior que 1, simplesmente decremetamos 1
  if($rootScope.paginaAtualID > 1) {
    $scope.urlPaginaAnterior += 'topico_'+$rootScope.topicoAtual.id+'/'+($rootScope.paginaAtualID - 1) ;
  }
  //se a página é 1, temos que voltar ao tópico anterior
  if($rootScope.paginaAtualID == 1 ){
    //só teremos tópico anterior se o atual não for o primeiro
    if(topicoAtual.id > 1 ){
      //decremetamos 1 para obter id do tópico anterior, entao decremetamos 1 novamente para obter o índice
      var indiceTopicoAnterior = topicoAtual.id-1-1;
      var topicoAnterior = topicos[indiceTopicoAnterior];
      //última página do tópico anterior
      $scope.urlPaginaAnterior += 'topico_'+topicoAnterior.id+'/'+topicoAnterior.quantidadePaginas
    }
    //se o topico atual não é maior que 1, então estamos na página 1 do tópico 1 e não precisamos do botão "RETORNAR"
    else if($rootScope.topicoAtual.id == 1 ){
      // TODO: link para unidade anterior
      //unidade atual é 1 ?
      $scope.anteriorDesativado = true;
      //se unidade atual é maior que 1
      //link é:  unidade atual - 1
    }
  }

  /**
  * url próxima página
  */
  $scope.urlProximaPagina = urlUnidade;
  //quando a página atual for a última página do tópico
  if($rootScope.paginaAtualID == topicoAtual.quantidadePaginas){
    //incrementamos 1 para obter o id do próximo tópico, então decremetamos 1 para obter o índice (apesar de +1 -1 se anularem, fica explícito o que está acontecendo)
    var indiceProximoTopico = topicoAtual.id + 1 - 1;
    var proximoTopico = topicos[indiceProximoTopico];
    // se de fato existir próximo tópico, url será a primeira página do próximo tópico, senão, não precisamos do botão "AVANÇAR"
    if(proximoTopico){
      $scope.urlProximaPagina += 'topico_'+proximoTopico.id+'/1'
    }else{
      // TODO: link para próxima unidade
      $scope.proximoDesativado = true;
    }
  }else{
    //se a página não for a última do tópico, simplesmente incrementamos
    $scope.urlProximaPagina += 'topico_'+topicoAtual.id+'/'+($rootScope.paginaAtualID+1)
  }
}]);

function progressbar($scope, $rootScope, $http, $routeParams){  
 

  $scope.totalPercents = 0;
    //Setar variáveis no rootScope, assim podem ser usadas em todos os outros controllers
    $rootScope.unidadeAtual = $rootScope.unidades[$routeParams.unidadeID-1];
    
    

    // se a url nao tiver topicoID, carregamos o sumario da unidade
  if($routeParams.topicoID){
    $rootScope.topicoAtual   = $rootScope.unidadeAtual.topicos[$routeParams.topicoID-1];
    var topicos = $rootScope.unidadeAtual.topicos;
    var topicoAtual = $rootScope.topicoAtual;

    $rootScope.topicoAtual    = $rootScope.unidadeAtual.topicos[$routeParams.topicoID-1];
    $rootScope.paginaAtualID  = parseInt($routeParams.paginaID);
    var obj = {};
    for(var i = 0; i < $rootScope.unidades.length; i++) {
        obj[i] = {
            qtdePags: 0
        };
        angular.forEach($rootScope.unidades[i].topicos, top => {
            obj[i].qtdePags += Number(top.quantidadePaginas);
            
        });
        
    }    
    
    $scope.qtTotalPaginas = 0;
    $.each(obj, function(index,unidade){
      $scope.qtTotalPaginas += parseInt(unidade.qtdePags);      
    });
    $scope.paginacaoAtual = 0;
    for (var i = 0; i< topicoAtual.id-1; i++){
      $scope.paginacaoAtual += parseInt(topicos[i].quantidadePaginas);
    }
    $scope.paginacaoAtual += $rootScope.paginaAtualID;
    

    var list = localStorage.getItem("read"+$rootScope.unidadeAtual.id+$rootScope.topicoAtual.id+$rootScope.paginaAtualID+"v");

    // Início barra de progresso
  $scope.totalPercents = localStorage.getItem("progressbar");
  
    if($scope.totalPercents){
      var valor = localStorage.getItem("valor");
      $scope.totalPercents = localStorage.getItem("progressbar");
      if(list == document.URL){
        $scope.totalPercents = valor;               
      }else if($scope.totalPercents >= valor){
        $scope.num = localStorage.getItem("num");
        $scope.percent = $scope.num * 100/ parseInt($scope.qtTotalPaginas);
        $scope.totalPercents = $scope.percent.toFixed(0);
        localStorage.setItem("progressbar", $scope.totalPercents);
        localStorage.setItem("num",  ((parseInt($scope.num)+1)));
        var valor = localStorage.setItem("valor", $scope.totalPercents);
        var listPages = localStorage.setItem("read"+$rootScope.unidadeAtual.id+$rootScope.topicoAtual.id+$rootScope.paginaAtualID+"v", document.URL);
      }else{
        console.log("Erro ao setar valor!");
      }
    }else{
    var num = 1;
    $scope.percent = 1 * 100 / $scope.qtTotalPaginas;
    $scope.totalPercents = $scope.percent.toFixed(0);   
    localStorage.setItem("progressbar", $scope.totalPercents);
    localStorage.setItem("num",  ((num+1)));
    var valor = localStorage.setItem("valor", $scope.totalPercents);
    var listPages = localStorage.setItem("read"+$rootScope.unidadeAtual.id+$rootScope.topicoAtual.id+$rootScope.paginaAtualID+"v", document.URL);
  }
    console.log($scope.totalPercents);
  }  
};

structure.controller('autoRefreshController',['$scope', '$route', '$interval', function($scope, $route, $interval) {
  $scope.totalPercents = localStorage.getItem("progressbar");
  var auto = $interval(function() {
    $scope.totalPercents = localStorage.getItem("progressbar");
    $state.reload("#progressbar");
  }, 8000,2);
}]);

/*
* Routers
*/
structure.config (['$routeProvider', function ($routeProvider) {

  $routeProvider
  .when ('/', {
    templateUrl: 'views/curso/sumario.html',
    controller: 'includeController'
  })
  // .when ('/material_de_estudos', {
  //   templateUrl: 'views/curso/sumario.html',
  //   controller: 'includeController'
  // })
  .when ('/creditos', {
    //templateUrl: 'views/curso/creditos.html',
    
    controller: 'includeController'
  })
  .when('/unidade_:unidadeID/topico_:topicoID/:paginaID', {
    templateUrl: 'views/curso/include.html',
    controller: 'includeController'
  })
  .when('/unidade_:unidadeID/:jogo/1', {
    templateUrl: 'views/curso/include.html',
    controller: 'includeController'
  })
  // nesse caso o includeController vai chamar o sumario da unidade
  .when('/unidade_:unidadeID',{
    templateUrl: 'views/curso/include.html',
    controller: 'includeController'
  })
  .otherwise ({
    redirectTo: '/'
  });
}]);

// Responsável pelo gerenciamento dos modais do curso
structure.directive('modal', function() {
  return {
    templateUrl: 'partials/modal.html',
    replace: true,
    restrict: 'E',
    transclude: true,
    scope: {
      target: "@target",
      titulo: "@titulo",
      tamanho: "@tamanho"
    }
  }
});